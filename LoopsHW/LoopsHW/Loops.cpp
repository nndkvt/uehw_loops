#include <iostream>

void FindEvenNumbers(int n) 
{
	std::cout << "All even numbers: ";

	for (int i(0); i <= n; i += 2) 
	{
		std::cout << i << " ";
	}
	
	std::cout << "\n";
}

void FindOddNumbers(int n) 
{
	std::cout << "All odd numbers: ";

	for (int i(1); i <= n; i += 2)
	{
		std::cout << i << " ";
	}

	std::cout << "\n";
}

void DisplayNumbers(int n) 
{
	std::cout << "All numbers: ";

	for (int i(0); i <= n; i++) 
	{
		std::cout << i << " ";
	}

	std::cout << "\n";
}