﻿#include <iostream>
#include "Loops.h"

int number = 10;

int main()
{
	DisplayNumbers(number);

	FindEvenNumbers(number);

	FindOddNumbers(number);

	return 0;
}
